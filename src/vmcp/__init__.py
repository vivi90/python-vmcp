#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Virtual Motion Capture (VMC) protocol package.

.. include:: ../../README.md

"""

__all__ = [
  "typing",
  "protocol",
  "events",
  "facades"
]
